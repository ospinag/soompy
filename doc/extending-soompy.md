Extending SOOMpy
================

If you find that the built-in elements SOOMpy provides are not enough to deal with your problem domain, you can always create a new element type.

To make sure your contribution will be usable by others (and will integrate well with existing classes), here's a few things to keep in mind.


### Visuals Matter

Have some way to graphically represent your class. This means having a `.plot()` method that works. Without a good visual representation, some users may find it difficult to use your work correctly.


### Two Ways to Display

Python provides \_\_str\_\_ and \_\_repr\_\_ as built-in ways of showing a text representation of an object. These methods can help users debug their models by showing them the values of member variables.

Remember:

 - **\_\_str\_\_** is meant for a quick, user-friendly summary of the most important information about an object. This method is called whenever the user decides to `print` an object.
 - **\_\_repr\_\_** is meant to be an unambiguous string representation of an object. As such, it's great for debug dumps of all of the data in an object.


