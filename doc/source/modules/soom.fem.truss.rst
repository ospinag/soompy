.. automodule:: soom.fem.truss

    Class Description
    -----------------
    .. autoclass:: Truss
       :members:
       :private-members:
       :special-members:
