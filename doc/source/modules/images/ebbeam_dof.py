# -*- coding: utf-8 -*-
"""
This script creates the graphs used for the documentation of soom.geometry.section

"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

# Constants
blen = 1.5         # Beam length
alength = 0.5      # Arrow length
amargin = 0.05     # Margin between arrows / arrows and beam
tipscale = 10      # Scale for the tip of the arrow
textmargin = 0.05  # Margin for text

# Create The beam
x = [0.0, blen]
y = [0.0, 0.0]
z = [0.0, 0.0]

plt.clf()
fig = plt.gcf()
ax = fig.gca(projection='3d')

# Plotting the beam
plt.plot(x,y,z, lw = 3)

# Plotting DOF node 1
# u_1
a = Arrow3D([-(alength + amargin), -amargin], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
plt.rc('text', usetex=True)
ax.text(-amargin, 0.0, textmargin, 'u_1', va = 'bottom', ha = 'right')

# v_1
a = Arrow3D([0.0, 0.0], [amargin, (alength + amargin)], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(0.0, alength + amargin, textmargin, 'v_1', va = 'bottom', ha = 'right')

# w_1
a = Arrow3D([0.0, 0.0], [0.0, 0.0], [amargin, (alength + amargin)], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(textmargin, 0.0, alength + amargin, 'w_1', va = 'top', ha = 'left')

# theta_x1
a = Arrow3D([-2*(alength + amargin), -2*amargin-alength], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([-2*(alength + amargin), -3*amargin-alength], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(-2*amargin-alength, 0.0, textmargin, '\\theta_{x1}', va = 'bottom', ha = 'right')

# theta_y1
a = Arrow3D([0.0, 0.0], [2*amargin + alength, 2*(alength + amargin)], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([0.0, 0.0], [2*amargin + alength, 2*alength + amargin], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(0.0, 2*(alength + amargin), textmargin, '\\theta_{y1}', va = 'bottom', ha = 'right')

# theta_z1
a = Arrow3D([0.0, 0.0], [0.0, 0.0], [2*amargin+alength, 2*(alength + amargin)], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([0.0, 0.0], [0.0, 0.0], [2*amargin+alength, 2*alength + amargin], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(textmargin, 0.0, 2*(alength + amargin), '\\theta_{z1}', va = 'top', ha = 'left')


# Plotting DOF of node 2
# u_2
a = Arrow3D([(blen + amargin), (blen + amargin + alength)], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text((blen + amargin + alength), 0.0, textmargin, 'u_2', va = 'bottom', ha = 'right')

# v_1
a = Arrow3D([blen, blen], [amargin, (alength + amargin)], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(blen, alength + amargin, textmargin, 'v_2', va = 'bottom', ha = 'right')

# w_1
a = Arrow3D([blen, blen], [0.0, 0.0], [amargin, (alength + amargin)], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(textmargin + blen, 0.0, alength + amargin, 'w_3', va = 'top', ha = 'left')

# theta_x2
a = Arrow3D([blen + alength + 2*amargin, blen + 2*(amargin+alength)], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([blen + alength + 2*amargin, blen + amargin + 2*alength], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(blen + 2*(amargin+alength), 0.0, textmargin, '\\theta_{x2}', va = 'bottom', ha = 'right')

# theta_y2
a = Arrow3D([blen, blen], [2*amargin + alength, 2*(alength + amargin)], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([blen, blen], [2*amargin + alength, 2*alength + amargin], [0.0, 0.0], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(blen, 2*(alength + amargin), textmargin, '\\theta_{y2}', va = 'bottom', ha = 'right')

# theta_z2
a = Arrow3D([blen, blen], [0.0, 0.0], [2*amargin+alength, 2*(alength + amargin)], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
a = Arrow3D([blen, blen], [0.0, 0.0], [2*amargin+alength, 2*alength + amargin], mutation_scale=tipscale, lw=1, arrowstyle="-|>", color="k")
ax.add_artist(a)
ax.text(blen+textmargin, 0.0, 2*(alength + amargin), '\\theta_{z2}', va = 'top', ha = 'left')


# Axis
# X-axis
a = Arrow3D([blen + 2*alength + 3*amargin, blen + 3*(amargin+alength)], [0.0, 0.0], [0.0, 0.0], mutation_scale=tipscale, lw=2, arrowstyle="->", color="k")
ax.add_artist(a)
ax.text(blen + 3*(amargin+alength), 0.0, textmargin, 'x', va = 'bottom', ha = 'right')

# Y-axis
a = Arrow3D([0.0, 0.0], [3*amargin + 2*alength, 3*(alength + amargin)], [0.0, 0.0], mutation_scale=tipscale, lw=2, arrowstyle="->", color="k")
ax.add_artist(a)
ax.text(0.0, 3*(alength + amargin), textmargin, 'y', va = 'bottom', ha = 'right')

# Z-axis
a = Arrow3D([0.0, 0.0], [0.0, 0.0], [3*amargin+2*alength, 3*(alength + amargin)], mutation_scale=tipscale, lw=2, arrowstyle="->", color="k")
ax.add_artist(a)
ax.text(textmargin, 0.0, 3*(alength + amargin), 'z', va = 'top', ha = 'left')

# Adjusting axis
ax.set_xlim3d (0, 2.2)
ax.set_ylim3d (-1, 1.2)
ax.set_zlim3d (-0.5, 1.7)

plt.draw()

# Remove axis and save figure
ax.set_axis_off()
plt.savefig('ebbeam_dof.png', bbox_inches='tight', transparent = True)