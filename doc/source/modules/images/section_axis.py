# -*- coding: utf-8 -*-
"""
This script creates the graphs used for the documentation of soom.geometry.section

"""
import matplotlib.pyplot as plt

# Create a figure of the cross section to show the y and z axis
x = [0.0, 1.0, 1.0, 0.6, 0.6, 1.0, 1.0, 0.0, 0.0, 0.4, 0.4, 0.0, 0.0]
y = [0.0, 0.0, 0.2, 0.2, 1.8, 1.8, 2.0, 2.0, 1.8, 1.8, 0.2, 0.2, 0.0]

plt.clf()
ax = plt.gca()
plt.fill(x,y,'b')


#Axis
ax.arrow(-0.5, 1.0, 2.0, 0.0, head_width = 0.05, head_length = 0.1, lw = 3, color = 'black')
ax.text(1.55, 1.08, 'y')
plt.arrow(0.5, -0.25, 0.0, 2.50, head_width = 0.05, head_length = 0.1, lw = 3, color = 'black')
ax.text(0.55, 2.3,'z')
# Setting the axis correctly
plt.axis('equal')
plt.axis([-0.79, 1.79, -0.5, 2.5])
ax.set_axis_off()
plt.savefig('section_axis.png', bbox_inches='tight', transparent = True)