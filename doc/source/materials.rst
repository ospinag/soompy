Materials
=========
The :mod:`soom.materials` module allows one to describe the material properties 
of objects.

.. toctree::
   :maxdepth: 2

   modules/soom.materials.isolinear
   modules/soom.materials.isobilinear