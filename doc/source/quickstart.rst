Quick start
~~~~~~~~~~~

Building a finite element model
===============================

The first step in creating a finite element model is to create some nodes.  Let's consider that we have three nodes.  The first one located at (0,0,0).  Only the first node is constrained while all the degrees of freedom for the other elements are free::

    # Creating nodes
    n = [Node([0,0,0],bc = [True]*6), \
         Node([0,0,1]), \
         Node([0,1,0])]

By default the boundary conditions for any node are free.  To plot the node one can use::

    n.plot()

For the case of a linear structure, the next step is to create a cross section and an insolinear element.  This can be performed using::

    # Creating the section.  In this example let's assume that only the area is needed (axial element)
    s = Section(area = 0.1, iz = 3.0)
    
    # Creating material
    m = Isolinear(name = "Steel",e = 2e11, rho = 1.0)

Now, we are ready to create some elements.  For example, if we are using Euler Bernouly beams three beams can be created using::

    # Creating the beam element
    bs = [Ebbeam(n,s,m),
          Ebbeam([n[1], n[2], n[0]],s,m), \
          Ebbeam([n[2], n[0], n[1]],s,m)]

Notice that the Ebbeam requires three nodes.  The first two nodes are to indicate the ends of the beam.  The third node is used to orient the cross section.

Finally, we can build a finite element model using the following code::

    st = FemModel(elementList = bs,name = 'Test')

Notice that so far we have not numbered the nodes.  This can be done automatically using the following code::

    st.numberNodes()

The previous command will number the nodes and creates the st.nodeList property.  Finally, we can get the stiffness and mass matrix of the structure using::

    K = st.globalk()
    M = st.globalm()
