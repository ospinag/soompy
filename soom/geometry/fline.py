# -*- coding: utf-8 -*-
"""
Fline
~~~~~

:synopsis: Handles finite lines in space.

The fline class provides support to basic methods for "linear" finite elements
such as :class:`soom.fem.ebbeam.Ebbeam`.  Two of the most important functions that
it provides is to provide plotting functionality and to calculate transformation
matrices.

.. _fline-coordinate-transformation:

Coordinate transformation
=========================

The :meth:`Fline.tmatrix` method calculates different transformation matrices depending on the 
number of nodes of the :class:`Fline`.  If the element has three nodes, the first 
two nodes define the ends of the line and the third node the orientation 
of the cross section.  If only two nodes are defined, and the element is not 
parallel to the :math:`z` axis, the orientation of the cross section is such 
that the axis defining the :math:`I_y` is in a plane parallel to the :math:`x-y` plane. 
If the element is parallel to the :math:`z` axis, the axis defining :math:`I_y` 
is parallel to the global :math:`z` axis.

Consider an element in the local coordiante system defined by the
unit vectors :math:`(i', j', k')`.  The global coordinate
system is given by the unit vectors :math:`(i,j,k)`.  The relationship between
the vector of displacements in the locall coordinate system 
:math:`(u',v',w')` and those in the global coordinate system
:math:`(u,v,w)` can be written using the equations:

.. math::
    
    u' = u(i \cdot i') + v(j \cdot i') + w(k \cdot i')
    
    v' = u(i \cdot j') + v(j \cdot j') + w(k \cdot j')
    
    w' = u(i \cdot w') + v(j \cdot w') + w(k \cdot w')
    
Three node fline
----------------

The vectors :math:`i'`, :math:`j'`, and :math:`k'` can be found
with the coordinates of the 3 nodes defining the element.  By definition the first
two nodes correspond to the beginning and end of the element, and the third node
defines the orientation of the cross section.  If the three nodes have coordiantes
:math:`(x_1, y_1, z_1)`, :math:`(x_2, y_2, z_2)`, and :math:`(x_3, y_3, z_3)`
respectively, the unit vectors :math:`\hat{i}`, :math:`\hat{j}`, and :math:`\hat{k}` 
are calculated using the following equations:

.. math::

    i' = \\left( {x_2 - x_1 \over L_x} \\right) i + \\left( {y_2 - y_1 \over L_x} \\right) j + \\left( {z_2 - z_1 \over L_x} \\right) k
    
    j' = \\left( {x_3 - x_1 \over L_y} \\right) i + \\left( {y_3 - y_1 \over L_y} \\right) j + \\left( {z_3 - z_1 \over L_y} \\right) k
    
    k' = i' \\times j'

where :math:`L_x = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2 + (z_2 - z_1)^2}` and
:math:`L_y = \sqrt{(x_3 - x_1)^2 + (y_3 - y_1)^2 + (z_3 - z_1)^2}`.  

The coordinate transformation for displacements and rotations at nodes 1 and 2
can be written using the equation:

.. math::

    \\left\\{ \\begin{matrix}
    u'_1 \\\\
    v'_1 \\\\
    w`_1 \\\\
    \\theta'_{x1} \\\\
    \\theta'_{y1} \\\\
    \\theta'_{z1} \\\\
    u'_2 \\\\
    v'_2 \\\\
    w`_2 \\\\
    \\theta'_{x2} \\\\
    \\theta'_{y2} \\\\
    \\theta'_{z2} \\\\
    \\end{matrix} \\right\\} = \\left[ \\begin{matrix}
    i \cdot i' & j \cdot i' & k \cdot i' & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    i \cdot j' & j \cdot j' & k \cdot j' & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    i \cdot k' & j \cdot k' & k \cdot k' & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & i \cdot i' & j \cdot i' & k \cdot i' & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & i \cdot j' & j \cdot j' & k \cdot j' & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & i \cdot k' & j \cdot k' & k \cdot k' & 0 & 0 & 0 & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 & i \cdot i' & j \cdot i' & k \cdot i' & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 & i \cdot j' & j \cdot j' & k \cdot j' & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 & i \cdot k' & j \cdot k' & k \cdot k' & 0 & 0 & 0 \\\\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & i \cdot i' & j \cdot i' & k \cdot i' \\\\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & i \cdot j' & j \cdot j' & k \cdot j' \\\\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & i \cdot k' & j \cdot k' & k \cdot k'
    \\end{matrix} \\right] \\left\\{ \\begin{matrix}
    u_1 \\\\
    v_1 \\\\
    w_1 \\\\
    \\theta_{x1} \\\\
    \\theta_{y1} \\\\
    \\theta_{z1} \\\\
    u_2 \\\\
    v_2 \\\\
    w_2 \\\\
    \\theta_{x2} \\\\
    \\theta_{y2} \\\\
    \\theta_{z2} \\\\
    \\end{matrix} \\right\\}

where :math:`\\theta'_{x}`, :math:`\\theta'_{y}`, and :math:`\\theta'_{z}` are the rotation about
the axis defined by :math:`i'`, :math:`j'`, and :math:`k'` respectively.
Therefore, the transformation matrix is defined as:

.. math::
    :label: T

    T = \\left[ \\begin{matrix}
    R & 0 & 0 & 0 \\\\
    0 & R & 0 & 0 \\\\
    0 & 0 & R & 0 \\\\
    0 & 0 & 0 & R
    \\end{matrix} \\right]

where

.. math::
    :label: R

    R = \\left[ \\begin{matrix}
    i \cdot i' & j \cdot i' & k \cdot i' \\\\
    i \cdot j' & j \cdot j' & k \cdot j' \\\\
    i \cdot k' & j \cdot k' & k \cdot k' 
    \\end{matrix} \\right]

Two node fline
--------------

If the :class:`fline` only has two nodes the orientation of the cross section will
be performed as followed:

* Elements that are not parallel to the global :math:`z` axis (or :math:`k`) are \
oriented such that the axis defining :math:`I_z` are in a plane parallel to the\
 :math:`x-y` plane.

* Elements that are pallel to the global :math:`z` axis are oriented such that \
the axis defining :math:`I_z` are parallel to :math:`i`.

Therefore, if :math:`\\| i' \\cdot k \\| \\neq 1`:

.. math::
    
    j' = {k \\times i' \\over \\| k \\times i' \\|}
    
if :math:`\\| i' \\cdot k \\| = 1`:

.. math::

    j' = {i \\times i' \\over \\| i \\times i' \\|}
    
The vector :math:`k'` is calculated using:

.. math::

    k' = i' \\times j'
    
The matrices :math:`T` and :math:`R` are defined as in equation :eq:`T` and :eq:`R`
respectively.
"""

import numpy as np

class Fline(object):
    """
    :class:`Fline` allows the creation of lines in 3D space. Two unique
    :class:`Node` instances are required to construct such a line.

    Attributes:
        nodeList (list):   List of nodes to build from.
        comment (str):     Optional comments.
        dircosines (list): Direction cosines.
        vhandle (list): List of objects for graphical representation
        
    """
    def __init__(self, nodeList, comment=""):
        """
        Constructor for Fline objects.

        Args:
            nodeList (list): List of nodes to build from.
            comment (str):   Optional comments.
            vhandle (list): List of objects for graphical representation
            
        """
        self.nodeList    = nodeList
        self.comment     = comment
        self.vhandle     = 0
        self.pairs       = self.buildPairs(self.nodeList)

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Fline:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n'
        out += "Dircosines = " + str(self.dircosines()) + '\n'
        out += "Comments: " + self.comment
        return out

    def length(self):
        """
        Calculates the distance between the two nodes.

        Returns:
            number. The distance between the two nodes.
        """
        # Calculates the length of the element
        return np.sqrt(np.square(self.nodeList[0].coords[0] - self.nodeList[1].coords[0]) + 
                       np.square(self.nodeList[0].coords[1] - self.nodeList[1].coords[1]) +
                       np.square(self.nodeList[0].coords[2] - self.nodeList[1].coords[2]))

    def dircosines(self):
        out = []
        for i in range(0, 3):
            out.append( (self.nodeList[1].coords[i] - self.nodeList[0].coords[i]) / self.length() )
        return out

    def buildPairs(self, nodeList):
        """
        Legacy code. May be deprecated in future versions.
        """
        out = []
        lastNode = None
        for n in nodeList:
            out.append( (n, lastNode) )
            lastNode = n
        return out

    def display(self):
        """
        Renders this Fline instance as a string.

        Returns:
            str. The string representation of this Fline instance.
        """
        return str(self)

    def plot(self, plotopt = 'b'):
        """
        Plots this Fline instance via Matplotlib.
        
        The Fline.vhandle property is updated with a reference to the object
        graphically representing the Fline.

        Returns:
            None.
        """
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt

        # Set projection on current figure to 3D.
        fig = plt.gcf()
        fig.gca(projection='3d')
        
        # Plot lines between each pair of nodes.
        for item in self.nodeList:
            item.plot()
        self.vhandle = plt.plot([self.nodeList[0].coords[0], self.nodeList[1].coords[0]], #[start-x, end-x]
                 [self.nodeList[0].coords[1], self.nodeList[1].coords[1]], #[start-y, end-y]
                 [self.nodeList[0].coords[2], self.nodeList[1].coords[2]],plotopt) #[start-z, end-z]


    def tmatrix(self):
        """
        Provides the transformation matrix for a finite line.  For detailed information
        about the transformation matrix see the :ref:`coordinate transofrmation <fline-coordinate-transformation>` 
        section
        
        Returns:
            T (numpy.array): Transformation matrix
        """
        # Defining the coordinates of the first and second node
        x1 = self.nodeList[0].coords[0]
        y1 = self.nodeList[0].coords[1]
        z1 = self.nodeList[0].coords[2]
        
        x2 = self.nodeList[1].coords[0]
        y2 = self.nodeList[1].coords[1]
        z2 = self.nodeList[1].coords[2]
        
        # Define i, j and k (global coordiante system)
        i = np.array([1.0, 0.0, 0.0])
        j = np.array([0.0, 1.0, 0.0])
        k = np.array([0.0, 0.0, 1.0])

        # Defining the i' vector        
        Lx = np.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)
        ii = np.array([x2-x1, y2-y1, z2-z1]) / Lx
        
        # If the element has thre nodes (or more):
        if len(self.nodeList) >= 3:
            # Define the coordinates of the third node
            x3 = self.nodeList[2].coords[0]
            y3 = self.nodeList[2].coords[1]
            z3 = self.nodeList[2].coords[2]
            
            # Defining the j' vector
            Ly = np.sqrt((x3-x1)**2 + (y3-y1)**2 + (z3-z1)**2)
            jj = np.array([x3-x1, y3-y1, z3-z1]) / Ly
            
        else:
            # The third node depends if the element is parallel to the
            # global 'z' axis or not
            if np.abs(np.dot(ii,k)) != 1.0:
                jj = np.cross(k,ii)
                jj = jj/np.linalg.norm(j)
            else:
                jj = np.cross(i,ii)
                jj = jj/np.linalg.norm(jj)
        
        # Calculate k'
        kk = np.cross(ii,jj)
        
        # Calculate R
        R = np.array([[np.dot(i,ii), np.dot(j,ii), np.dot(k,ii)],
        [np.dot(i,jj), np.dot(j,jj), np.dot(k,jj)],
        [np.dot(i,kk), np.dot(j,kk), np.dot(k,kk)]])

        T = np.zeros(shape=(12,12));
        
        T[0:3,0:3] = R;
        T[3:6,3:6] = R;
        T[6:9,6:9] = R;
        T[9:12,9:12] = R;        

        return T

# Tests __str__ method.
if __name__ == '__main__':
    a = Node([0,0,0])
    b = Node([1,2,3])

    f = Fline([a,b])

    print f

    # plotting test
    fig = plt.figure()
    ax = Axes3D(fig) 
    f.plot()
    plt.show()
