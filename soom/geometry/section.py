# -*- coding: utf-8 -*-
"""
Section
~~~~~~~

:synopsis: Handles cross-sections.


Using Sections
--------------

Sections allow one to model cross-sectional areas in SOOMpy. A Section can be
created like so:

>>> s = Section(area=0.1)

This creates a section with an area of 0.1 units^2.  The moment of inertia
is equal to zero by default.

Unfortunately, Sections don't have a graphical representation at this time.
This means that calling :meth:`Section.plot` does nothing.

To change a Section's properties, just reassign any of its public member
variables. For example, to change the area a Section represents, one might
do something like:

>>> s.area = 5.0     # Section s now has an area of 5 units^2.

To see the values stored in a Section, one has to call its
:meth:`Section.__str__` method. The easiest way to do this is to just
``print`` the Section in question (``print`` calls :meth:`Section.__str__`
behind the scenes). For example:

>>> print s
Section:
Area = 5
Iy = 0
Iz = 0
J = 0
Asy = 0
Asz = 0
Comments:

A user can check if the properties assigned to a section are as expected by the
developers by invoking the method :meth:`Section.check`.  This method is only
to help users get familiar with the use of the objects and it is not called
before any calculations.  :meth:`Section.check` returns True if the check
passes.  A message is printed in the case that the check does not pass.

Axis of the cross section
-------------------------

.. image:: images/section_axis.png

The definition of the axis of the cross section is define above.  The :attr:`Section.iy`
describes the moment of inertia about the :math:`y` axis.  Similarly, :attr:`Section.iz`
describes the moment of inertia about hte "math"`z` axis.

"""


class Section:
    """
    :class:`Section` is used to represent cross-sectional areas for
    :class:`Ebbeam` and other 3D objects.

    Attributes:
        name (str):    Section name
        area (float):  Surface area.
        iy (float):    Moment of inertia of the cross section about the y axis
        iz (float):    Moment of inertia of the cross section about the z axis
        j (float):     Polar moment of inertia
        asy (float):   Asy.
        asz (float):   Asz.
        comment (str): Optional comments.
    """
    def __init__(self,
                 name="",
                 area=0.0,
                 iy=0.0,
                 iz=0.0,
                 j=0.0,
                 asy=1.0,
                 asz=1.0,
                 comment=""):
        """
        Constructor for Section objects.

        Args:
            name (str):    Section's name
            area (float):  Surface area.
            iy (float):    Iy.
            iz (float):    Iz.
            j (float):     J.
            asy (float):   Asy.
            asz (float):   Asz.
            comment (str): Optional comments.
        """
        self.name = name
        self.area = area
        self.iy   = iy
        self.iz   = iz
        self.j    = j
        self.asy  = asy
        self.asz  = asz
        self.comment = comment

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Section: " + self.name + '\n'
        out += "Area = " + str(self.area) + '\n'
        out += "Iy = " + str(self.iy) + '\n'
        out += "Iz = " + str(self.iz) + '\n'
        out += "J = " + str(self.j) + '\n'
        out += "Asy = " + str(self.asy) + '\n'
        out += "Asz = " + str(self.asz) + '\n'
        out += "Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this Section instance as a string.

        Returns:
            str. The string representation of this Section instance.
        """
        return str(self)

    def plot(self):
        """
        Not implemented.
        """
        pass

    def check(self):
        
        checkFlag = True
        
        # Check if the Area is a float
        if type(self.area) is not float:
            print "The section.area is expected to be a float (id %i)" % id(self)
            checkFlag = False
        
        # Check if Iy is double
        if type(self.iy) is not float:
            print "The section.iy is expected to be a float (id %i)" % id(self)
            checkFlag = False
            
        # Check if Iz is double
        if type(self.iz) is not float:
            print "The section.iz is expected to be a float (id %i)" % id(self)
            checkFlag = False
        
        # Check if J is double
        if type(self.j) is not float:
            print "The section.j is expected to be a float (id %i)" % id(self)
            checkFlag = False
        
        return checkFlag

# Tests __str__ method.
if __name__ == '__main__':
    a = Section("Column #1", 0, 0, 0, 0, 0, 0)
    print a
