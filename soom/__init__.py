# -*- coding: utf-8 -*-
"""
    __init__.py
    ~~~~~~~~~~~

    Package import file.

    :copyright: Copyright 2013 by Philip Conrad, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""


VERSION = (0, 0, 1, 'alpha', 0)

import fem as fem
import geometry as geometry
import materials as materials