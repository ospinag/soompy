# -*- coding: utf-8 -*-
"""
FemModel
~~~~~~~~

:synopsis: Handles model structures.
"""

import scipy.sparse as sp
import numpy as np


class FemModel:
    """
    :class:`FemModel` is used to model structures.

    Attributes:
        elements (list): List of structural elements to build from.
        name (str): Name of this structure.
    """
    def __init__(self,
                 elementList,
                 name,
                 nodeList=[]):
        """
        Constructor for FemModel objects.

        Args:
            elementList (list): List of structural elements to build from.
            name (str): Name of this structure.
        """
        self.elementList = elementList
        self.name = name
        self.nodeList = nodeList

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = self.name + "\n"
        out += "Elements: " + str(len(self.elementList))
        return out

    def display(self):
        """
        Renders this FemModel instance as a string.

        Returns:
            str. The string representation of this FemModel instance.
        """
        return str(self)

    def plot(self):
        """
        Plots this FemModel instance via Matplotlib.

        Returns:
            None.
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D # flake8: noqa

        # Set projection on current figure to 3D.
        fig = plt.gcf()
        fig.gca(projection='3d')

        # Plot element that has a plot method.
        for item in self.elementList:
            if "plot" in dir(item):
                item.plot()

    def numberNodes(self):
        """
        Extract all the nodes from the structural elements and renumber them

        Returns:
            None.
        """
        self.nodeList = []
        nodeNumber = 0

        # For each element in the list of elements
        for element in self.elementList:
            # Checking if the element has a property called "nodeList"
            if "nodeList" in dir(element):
                # For each of the nodes in the nodeList for this element
                for proposednode in element.nodeList:
                    # Check if the node is not already in the femmodel nodeList
                    if proposednode not in self.nodeList:
                        # Change the node number and add it to the femmodel.nodeList
                        proposednode.number = nodeNumber
                        self.nodeList.append(proposednode)
                        nodeNumber += 1

    def constMat(self):
        """
        Calculates the constrain matrix

        Returns:
            T (scipy.sparse.csc_matrix): Matrix that applies constrains
        """
        # List with all the bcs of nodes sorted
        ndof = len(self.nodeList) * 6
        bcs = []
        [bcs.extend(item.bc[:]) for item in sorted(self.nodeList, key=lambda node: node.number)]

        # Creating the transformation matrix
        rows = range(ndof)
        for j in range(ndof):
            if bcs[j]:
                rows.remove(j)

        cols = range(len(rows))
        vals = [1] * len(rows)

        T = sp.coo_matrix((vals, (rows, cols)),shape=(ndof, len(rows)))
        T = T.tocsc()

        return T

    def globalk(self, bc=True):
        """
        Calculates the stiffness matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            K (scipy.sparse.csc_matrix): Global stiffness matrix of the finite element model
        """

        # Initializing the stiffness matrix
        ndof = len(self.nodeList) * 6

        # scipy recommends building the matrix in lil or coo format and then
        # convert it to CSR or CSC for operations

        rows = []
        cols = []
        vals = []
        for element in self.elementList:
            # Check that the element an calculate a stiffness matrix
            if "globalk" in dir(element):
                # Get elemental matrix in global coordinates
                K_ele = element.globalk()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(K_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [K_ele[row, col] for row, col in zip(rowsele, colsele)]

        K = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        K = K.tocsc()

        # Applying boundary conditions
        if bc:
            T = self.constMat()
            K = T.transpose() * K * T
            K = K.tocsc()

        return K

    def check(self):
        """
        Check if the parameters of the FemModel were set correctly

        Returns:
            checkFlag (boolean): True if the point parameters are as expected

        This method calls the `check()` method in all the elements in
        elementList that have the method implemented.  This method also calls
        the `check()` method for all the nodes in nodeList if the list is
        not empty.
        """
        checkFlag = True

        # Check if the elementList is actually a list
        if type(self.elementList) is not list:
            print "The FemModel.elementList is not a list (id %i)" % id(self)
            checkFlag = False
        else:
            # Run a check in each element that has the check method implemented
            for element in self.elementList:
                if "check" in dir(element):
                    if not element.check():
                        checkFlag = False

        if len(self.nodeList) > 0:
            if not all([node.check() for node in self.nodeList]):
                checkFlag = False

        # self.elementList = elementList
        # self.nodeList = nodeList

        # Checking that the element
        return checkFlag

    def globalm(self, bc=True):
        """
        Calculates the mass matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            M (scipy.sparse.csc_matrix): Global mass matris of the finite element model
        """

        # Initializing the mass matrix
        ndof = len(self.nodeList) * 6

        rows = []
        cols = []
        vals = []

        for element in self.elementList:
            # Check that the element an calculate a stiffness matrix
            if "globalm" in dir(element):
                # Get elemental matrix in global coordinates
                M_ele = element.globalm()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(M_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [M_ele[row, col] for row, col in zip(rowsele, colsele)]

        M = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        M = M.tocsc()
        if bc:
            T = self.constMat()
            M = T.transpose() * M * T
            M = M.tocsc()

        return M

    def globalc(self, bc=True):
        """
        Calculates the damping matrix of the finite element model

        Args:
            bc (boolean): True if boundary conditions should be applied.
                          False returns the matrix without constrains applied

        Returns:
            C (scipy.sparse.csc_matrix): Global damping matrix of the finite element model
        """

        # Initializing the stiffness matrix
        ndof = len(self.nodeList) * 6

        # scipy recommends building the matrix in lil or coo format and then
        # convert it to CSR or CSC for operations

        rows = []
        cols = []
        vals = []
        for element in self.elementList:
            # Check that the element an calculate a damping matrix
            if "globalc" in dir(element):
                # Get elemental matrix in global coordinates
                C_ele = element.globalc()
                # Ask the lement what DOF do the element contribute
                dofList = element.dofList()
                # Getting the info needed to build structure's matrix
                rowsele, colsele = np.where(C_ele)
                rows += [dofList[item] for item in rowsele]
                cols += [dofList[item] for item in colsele]
                vals += [C_ele[row, col] for row, col in zip(rowsele, colsele)]

        C = sp.coo_matrix((vals, (rows, cols)), shape=(ndof, ndof))
        C = C.tocsc()

        # Applying boundary conditions
        if bc:
            T = self.constMat()
            C = T.transpose() * C * T
            C = C.tocsc()

        return C
        
