# -*- coding: utf-8 -*-
"""
Lmass
~~~~~

:synopsis: Handles lumped mass objects.


Using Lmass
------------

Lmass are used to model lumped masses in a finite element model.
To build a Lbeam, one needs a list of Nodes, and a list of doubles (size 6x1)
indicating the mass added to each degree of freedom.  For example:

>>> lm = Lmass([Node([0,0,0]), Node([1,1,1])],[10, 10, 10, 0, 0, 0])

This creates a Lmass at two new nodes at coordinates (0, 0, 0) and (1,1,1).
By convention, the degrees of freedom for the masses are in the following
order: displacement along the X-axis; displacement along the Y-axis;
displacement in the Z-axis; rotation about the X-axis; rotation about the
Y-azis; and rotation about the Z-axis.  The additional mass added to the nodes
is the same and equal to [10, 10, 10, 0, 0, 0] for the example above.

Lmass has a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`Lmass.plot` method of an Ebbeam. For example:

>>> lm.plot()

IMAGE MISSING - ADD IMAGE

Properties of a Lmass may change over time. The node a lumped mass is
assigned to can be changing by changing the :attr:`Lmass.node` member variable.
For example:

>>> lm.node[0] = Node([1,2,3])    # changes the first node of Lmass.

To see the nodes and values stored in a Lmass, one has to call its
:meth:`Lmass.__str__` method. The easiest way to do this is to just ``print``
the Lmass object (``print`` calls :meth:`Lmass.__str__` behind the
scenes). For example:

>>> print lm
Lmass:
Node: 1
- Node:
    X = 0
    Y = 0
    Z = 0
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Node:
    X = 1
    Y = 1
    Z = 1
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [0, 0, 0, 0, 0, 0]
    Comments:
- Mass = [10, 10, 10, 0, 0, 0]
Comments:

"""

import numpy as np
from .node import Node


class Lmass():
    """
    :class:`Lmass` is used to model lumped masses in a finite element model.

    Attributes:
        nodeList (list): List of nodes with the same lumped mass distribution.
        mass (Isolinear): 6x1 list of doubles with lumped mass values (1 for each DOF)
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 mass,
                 comment=""):
        """
        Constructor for Lmass objects.

        Args:
            nodeList (list): List of nodes with the lumped mass.
            mass (list): Lumped mass (6x1 list).
            comment (str): Optional comments.
        """

        # Assigning values to the properties
        self.nodeList = nodeList
        self.mass = mass
        self.comment = comment

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Lmass:\n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Mass = ["
        for item in [str(x) for x in self.mass]:
            out += item + ', '
        out = out[:-2] + ']\n'
        out += "Comments: " + self.comment
        return out

    def check(self):
        """
        Checks consistency in the Lmass object.

        Returns:
            checked (boolean): True indicates that the object passes the check
            false indicates that the object does not pass the test.

        In addition to return a boolean, the command prints on the screen any
        problems found with the Lmass.  For example:

        >>>> lm.check()
        The len(Lmass.mass) should be equal to 6 (id = 122598144)

        The *id* provided in parenthesis corresponds to the id of the object
        with the problem.

        """
        checked = True

        # Checking that the mass list is the right length
        if len(self.mass) != 6:
            checked = False
            print 'The len(Lmass.mass) should be equal to 6 (id = ' + str(id(self)) + ')'

        # Checking that the values on self.mass are double
        if not all([isinstance(item, float) for item in self.mass]):
            checked = False
            print 'The items in the list Lmass.mass should be float (id = ' + str(id(self)) + ")"

        # Check that the nodeList is a list
        if not isinstance(self.nodeList, list):
            checked = False
            print 'Lmass.nodeList should be a list (id = ' + str(id(self)) + ')'
        else:
            # Check that all elements in self.nodeList are nodes
            if not all([isinstance(item, Node) for item in self.nodeList]):
                checked = False
                print 'The items in Lmass.nodeList should be an instance of Node (id = ' + str(id(self)) + ')'

        return checked

    def display(self):
        """
        Renders this Lmass instance as a string.

        Returns:
            str. The string representation of this Lmass instance.
        """
        return str(self)

    def globalm(self):
        """
        Computes the mass matrix of the Lmass object.

        Returns:
            m (numpy.matrix): The local mass matrix for all the elements.  For
            example, if Lmass.nodeList is of size *n*, the returned mass matrix
            is of size *6n x 6n*
        """
        m = np.diag(np.double(self.mass * len(self.nodeList)))
        return m

    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and stiffness

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*6, self.nodeList[0].number*6+6)
        dof += range(self.nodeList[1].number*6, self.nodeList[1].number*6+6)
        return dof
