# -*- coding: utf-8 -*-
"""
Lspring
~~~~~~~

:synopsis: Handles linear spring objects.


Using lspring
-------------

lspring is used to model springs in 3D space.
To build an lspring, one needs a list of two end Nodes and the stiffness 6x1 vector for the dof of the spring, for example:

>>> nlist = [Node([0,1,3]), Node([0,5,1])]
>>> sstif = [100, 100, 100, 100, 100, 100]
>>> spring = Lspring(nlist, sstif)

This creates an spring from the list of provided Nodes. The spring has three traslational and three rotational stiffness values. Values are in local coordinates.

lspring has a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`lspring.plot` method of an lspring. For example:

>>> lspring.plot()

.. image:: images/lspring_dof.png

Properties of a lspring may change over time. The nodes a spring is constructed
from can be changed by swapping out one node for another. This can be done by
indexing into the :attr:`lspring.nodelist` member variable, for example:

>>> spring.nodeList[2] = Node([1,2,3])   # changes the node used for orientation.

To see the nodes and values stored in an lspring, one has to call its
:meth:`lspring.__str__` method. The easiest way to do this is to just ``print``
the lspring in question (``print`` calls :meth:`lspring.__str__` behind the
scenes). For example:

>>> print spring
lspring: 
Nodes: 2
- Node:
    X = 0
    Y = 1
    Z = 3
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [False, False, False, False, False, False]
    Number = []
    Comments: 
- Node:
    X = 0
    Y = 5
    Z = 1
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [False, False, False, False, False, False]
    Number = []
    Comments: 
Length = 4.472135955
------
Stiffness in each axis (local coordiantes): [100, 100, 100, 100, 100, 100]
------
lspring Comments: 

Degrees of freedom
------------------

.. image:: images/ebbeam_dof.png

The degrees of freedom of the spring element are described by the vector:

.. math::

    d^T = \\left\\{ \\begin{matrix}
    u_1, v_1, w_1, \\theta_{x1}, \\theta_{y1}, \\theta_{z1}, u_2, v_2, w_2, \\theta_{x2}, \\theta_{y2}, \\theta_{z2}
    \\end{matrix} \\right\\}^T

where :math:`u` correspond to displacement in the :math:`x` direction; :math:`v`
indicates displacement in the :math:`y` direction; and :math:`w` corresponds
to displacement in the :math:`z` direction.  :math:`\\theta_x`, :math:`\\theta_y`, and :math:`\\theta_z`
are the rotations about the :math:`x`, :math:`y`, and :math:`z` axis respectively.
The sub-index (1 or 2) indicate the first or second node from :attr:`Truss.nodeList`.

Stiffness matrix
----------------

The stiffness matrix in local coordinates can be calculated using :meth:`lspring.localk`.
The local stiffness matrix is given by the equation [Cook_et_al_2007]_ :

.. math::

    K =\\left[  \\begin{matrix}    
    0 & {ky} & 0 & 0 & 0 & 0 & 0 & {-ky} & 0 & 0 & 0 & 0 \\\\
    0 & 0 & {kz} & 0 & 0 & 0 & 0 & 0 & {-kz} & 0 & 0 & 0 \\\\
    0 & 0 & 0 & {krx} & 0 & 0 & 0 & 0 & 0 & {-krx} & 0 & 0 \\\\
    0 & 0 & 0 & 0 & {kry} & 0 & 0 & 0 & 0 & 0 & {-kry} & 0 \\\\
    0 & 0 & 0 & 0 & 0 & {krz} & 0 & 0 & 0 & 0 & 0 & {-krz} \\\\
    {-kx} & 0 & 0 & 0 & 0 & 0 & {kx} & 0 & 0 & 0 & 0 & 0 \\\\
    0 & {-ky} & 0 & 0 & 0 & 0 & 0 & {ky} & 0 & 0 & 0 & 0 \\\\
    0 & 0 & {-kz} & 0 & 0 & 0 & 0 & 0 & {kz} & 0 & 0 & 0 \\\\
    0 & 0 & 0 & {-krx} & 0 & 0 & 0 & 0 & 0 & {krx} & 0 & 0 \\\\
    0 & 0 & 0 & 0 & {-kry} & 0 & 0 & 0 & 0 & 0 & {kry} & 0 \\\\
    0 & 0 & 0 & 0 & 0 & {-krz} & 0 & 0 & 0 & 0 & 0 & {krz} \\\\
    \\end{matrix} \\right]

Where :math:`kx`, :math:`ky`, :math:`kz` are the translational stifnesses in the 
local :math:`x`, :math:`y`, and :math:`z` axis.  :math:`krx`, :math:`kry`, and 
:math:`krz` are the rotational stiffness about the local :math:`x`, :math:`y`, and 
:math:`z` axis respectively.
"""

import numpy as np
from ..geometry.fline import Fline

class Lspring(Fline):
    """
    :class:`lspring` is used to model linear springs in structures.

    Attributes:
        nodeList (list): List of nodes to build from.
        stiffness (list): 6x1 list with the value of stiffness for each DOF (3 translations and 3 rotations). These values are in the local coordinate system.
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 stiffness,
                 comment=""):
        """
        Constructor for lspring objects.

        Args:
            nodeList (list): List of nodes to build from.
            stiffness (list): 6x1 Stiffness.
            comment (str): Optional comments.
        """
        super(Lspring, self).__init__(nodeList=nodeList,  comment=comment)
        self.stiffness  = stiffness
     
    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "lspring: \n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n------\n'
        out += "Stiffness in each axis (local coordiantes): "+ str(self.stiffness) + '\n------\n'
        out += "lspring Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this lspring instance as a string.

        Returns:
            str. The string representation of this spring instance.
        """
        return str(self)

    def localk(self):
        """
        Computes the local stiffnes matrix of the spring.

        Returns:
            k (numpy.array): The local stiffness matrix.
        """
        # Stiffness properties
        ks = self.stiffness
        
        k = np.zeros(shape=(12, 12))

        for n in range(0,6):
            k[n, n]   = ks[n]
            k[n+6, n+6]   = ks[n]
            k[n, n+6]   = -ks[n]
            k[n+6, n]   = -ks[n]
        
        return k

    def globalk(self):
        """
        Calculates the global stiffness matrix

        Returns:
            K (scipy.array): Global stiffness matrix
        """
        # Calculating transformation and local stiffness matrix
        T = self.tmatrix()
        k = self.localk()

        # Calculate global stiffness matrix
        K = np.dot(np.dot(T.T, k), T)

        return K

    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and stiffness

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*6, self.nodeList[0].number*6+6)
        dof += range(self.nodeList[1].number*6, self.nodeList[1].number*6+6)
        return dof

