# -*- coding: utf-8 -*-
"""
Vdamper
~~~~~~~

:synopsis: Handles linear damper objects.


Using Vdamper
-------------

Vdamper is used to model dampers in 3D space.  To build an Vdamper, one needs 
a list of two end Nodes and the damping 6x1 vector for the dof of the damper, 
for example:

>>> from soom.fem.node import Node
>>> from soom.fem.vdamper import Vdamper
>>> nlist = [Node([0,1,3]), Node([0,5,1])]
>>> c = [100, 100, 100, 100, 100, 100]
>>> damper = Vdamper(nlist, c)

This creates an damper from the list of provided Nodes. The damper has three 
traslational and three rotational damping values. Values are in local coordinates.

Vdamper has a graphical representation, which can be handy for visualizing
what a structure looks like. To show this representation, one can call the
:meth:`Vdamper.plot` method of an Vdamper. For example:

>>> damper.plot()

.. image:: images/vdamper_dof.png

Properties of a Vdamper may be changed. For example, the nodeList defining the
damper can be changed. This can be done by adding an orientation node
to :attr:`Vdamper.nodelist`:

>>> damper.nodeList.append(Node([1,2,3]))   # A third node to orient the damper.

To see the nodes and values stored in an Vdamper, one has to call its
:meth:`Vdamper.__str__` method. The easiest way to do this is to just ``print``
the Vdamper in question (``print`` calls :meth:`Vdamper.__str__` behind the
scenes). For example:

>>> print damper
Vdamper: 
Nodes: 2
- Node:
    X = 0
    Y = 1
    Z = 3
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [False, False, False, False, False, False]
    Number = []
    Comments: 
- Node:
    X = 0
    Y = 5
    Z = 1
    Deformation = [0, 0, 0, 0, 0, 0]
    Cdof = [False, False, False, False, False, False]
    Number = []
    Comments: 
Length = 4.472135955
------
damping in each axis (local coordiantes): [100, 100, 100, 100, 100, 100]
------
Vdamper Comments: 

Degrees of freedom
------------------

.. image:: images/ebbeam_dof.png

The degrees of freedom of the damper element are described by the vector:

.. math::

    d^T = \\left\\{ \\begin{matrix}
    u_1, v_1, w_1, \\theta_{x1}, \\theta_{y1}, \\theta_{z1}, u_2, v_2, w_2, \\theta_{x2}, \\theta_{y2}, \\theta_{z2}
    \\end{matrix} \\right\\}^T

where :math:`u` correspond to displacement in the :math:`x` direction; :math:`v`
indicates displacement in the :math:`y` direction; and :math:`w` corresponds
to displacement in the :math:`z` direction.  :math:`\\theta_x`, :math:`\\theta_y`, and :math:`\\theta_z`
are the rotations about the :math:`x`, :math:`y`, and :math:`z` axis respectively.
The sub-index (1 or 2) indicate the first or second node from :attr:`Truss.nodeList`.

damping matrix
----------------

The damping matrix in local coordinates can be calculated using :meth:`Vdamper.localc`.
The local damping matrix is given by the equation:

.. math::

    C =\\left[  \\begin{matrix}    
    0 & {cy} & 0 & 0 & 0 & 0 & 0 & {-cy} & 0 & 0 & 0 & 0 \\\\
    0 & 0 & {cz} & 0 & 0 & 0 & 0 & 0 & {-cz} & 0 & 0 & 0 \\\\
    0 & 0 & 0 & {crx} & 0 & 0 & 0 & 0 & 0 & {-crx} & 0 & 0 \\\\
    0 & 0 & 0 & 0 & {cry} & 0 & 0 & 0 & 0 & 0 & {-cry} & 0 \\\\
    0 & 0 & 0 & 0 & 0 & {crz} & 0 & 0 & 0 & 0 & 0 & {-crz} \\\\
    {-cx} & 0 & 0 & 0 & 0 & 0 & {cx} & 0 & 0 & 0 & 0 & 0 \\\\
    0 & {-cy} & 0 & 0 & 0 & 0 & 0 & {cy} & 0 & 0 & 0 & 0 \\\\
    0 & 0 & {-cz} & 0 & 0 & 0 & 0 & 0 & {cz} & 0 & 0 & 0 \\\\
    0 & 0 & 0 & {-crx} & 0 & 0 & 0 & 0 & 0 & {crx} & 0 & 0 \\\\
    0 & 0 & 0 & 0 & {-cry} & 0 & 0 & 0 & 0 & 0 & {cry} & 0 \\\\
    0 & 0 & 0 & 0 & 0 & {-crz} & 0 & 0 & 0 & 0 & 0 & {crz} \\\\
    \\end{matrix} \\right]

Where :math:`cx`, :math:`cy`, :math:`cz` are the translational stifnesses in the 
local :math:`x`, :math:`y`, and :math:`z` axis.  :math:`crx`, :math:`cry`, and 
:math:`crz` are the rotational damping about the local :math:`x`, :math:`y`, and 
:math:`z` axis respectively.
"""

import numpy as np
from ..geometry.fline import Fline

class Vdamper(Fline):
    """
    :class:`Vdamper` is used to model linear dampers in structures.

    Attributes:
        nodeList (list): List of nodes to build from.
        damping (list): 6x1 list with the value of damping constant (c) for each DOF (3 translations and 3 rotations in local coordinates).
        comment (str): Optional comments.
    """
    def __init__(self,
                 nodeList,
                 damping,
                 comment=""):
        """
        Constructor for  Vdamper objects.

        Args:
            nodeList (list): List of nodes to build from.
            damping (list): 6x1 damping.
            comment (str): Optional comments.
        """
        super(Vdamper, self).__init__(nodeList=nodeList,  comment=comment)
        self.damping  = damping
     
    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = "Vdamper: \n"
        out += "Nodes: " + str(len(self.nodeList))
        # Text wizardry to indent Points in output.
        for item in [str(x) for x in self.nodeList]:
            out += "\n- " + item.replace('\n', '\n    ')
        out += '\n'
        out += "Length = " + str(self.length()) + '\n------\n'
        out += "damping in each axis (local coordiantes): "+ str(self.damping) + '\n------\n'
        out += "Vdamper Comments: " + self.comment
        return out

    def display(self):
        """
        Renders this Vdamper instance as a string.

        Returns:
            str. The string representation of this damper instance.
        """
        return str(self)

    def localc(self):
        """
        Computes the local damping matrix for the damper.

        Returns:
            c (numpy.array): The damping matrix in local coordinates.
        """
        # damping properties
        cs = self.damping
        
        c = np.zeros(shape=(12, 12))

        for n in range(0,6):
            c[n, n]   = cs[n]
            c[n+6, n+6]   = cs[n]
            c[n, n+6]   = -cs[n]
            c[n+6, n]   = -cs[n]
        
        return c

    def globalc(self):
        """
        Calculates the global damping matrix

        Returns:
            c (scipy.array): Global damping matrix
        """
        # Calculating transformation and local damping matrix
        T = self.tmatrix()
        c = self.localc()

        # Calculate global damping matrix
        c = np.dot(np.dot(T.T, c), T)

        return c

    def dofList(self):
        """
        Determines the DOF that contribute to the element's mass and damping

        Returns:
            dof (list): List of DOF
        """
        dof = range(self.nodeList[0].number*6, self.nodeList[0].number*6+6)
        dof += range(self.nodeList[1].number*6, self.nodeList[1].number*6+6)
        return dof

