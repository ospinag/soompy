# -*- coding: utf-8 -*-
"""
Isobilinear
===========

:synopsis: Handles isobilinear materials.
"""


class Isobilinear:
    """
    :class:`Isobilinear` allows the creation of isobilinear materials.

    Note:
        At this time, dependent properties are set when the object is created.
        In the future, this may be replaced by methods that compute the
        dependent properties on-demand.

    Warning:
        Some methods, namely :meth:`display`, and :meth:`plot` are
        not implemented yet.

    Attributes:
        name (str):  The display name for this material.
        rho (float): Rho of this material.
        e (float):   Elasticity of this material.
        nu (float):  Nu of this material.
        g (number):  G.
        fy (number): Fy.
        fu (number): Fu.
        alpha (number): Alpha.
        e2 (number): E2.
        beta (number): Beta.
        eu (number): Eu.
    """
    def __init__(self, name="unknown", rho=0, e=0, nu=0,
                 fy=0, fu=0, alpha=0, beta=0):
        """
        Constructor for Isobilinear objects.

        Args:
            name (str):  The display name for this material.
            rho (float): Rho of this material.
            e (float):   Elasticity of this material.
            nu (float):  Nu of this material.
            fy (number): Fy.
            fu (number): Fu.
            alpha (number): Alpha.
            beta (number): Beta.
        """
        self.name  = name
        self.rho   = rho
        self.e     = e
        self.nu    = nu
        self.g     = e / (2 * (nu + 1))  # dependent property
        self.fy    = fy
        self.fu    = fu
        self.alpha = alpha
        self.e2    = e * alpha  # dependent property
        self.beta  = beta
        self.eu    = e * beta  # dependent property

    def __str__(self):
        """
        Creates a human-readable printout of the object's parameters.

        Returns:
            out (str): Output string.
        """
        out = ""
        out += "Material Name = " + self.name  + '\n'
        out += "Type = isobilinear\n"
        out += "rho = " + str(self.rho) + '\n'
        out += "e   = " + str(self.e) + '\n'
        out += "nu  = " + str(self.nu) + '\n'
        out += "g   = " + str(self.g) + '\n'
        out += "fy  = " + str(self.fy) + '\n'
        out += "fu  = " + str(self.fu) + '\n'
        out += "alpha = " + str(self.alpha) + '\n'
        out += "e2  = " + str(self.e2) + '\n'
        out += "beta  = " + str(self.beta) + '\n'
        out += "eu  = " + str(self.nu) + '\n'
        return out

    def display(self):
        """
        Not implemented.
        """
        pass

    def plot(self):
        """
        Not implemented.
        """
        pass
