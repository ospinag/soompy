SOOMpy
======

Structural Object-Oriented Modelling in Python.


Requirements
------------

 - [numpy][req-numpy]
 - [scipy][req-scipy]
 

Install
-------

To install SOOMpy as a python module on your system, run this from the top-level directory:

    python setup.py install

Or, using [pip][pip]:

    pip install soom

To install optional dependencies, pip provides a convenient syntax:

For documentation:

    pip install -e .[doc]

For plotting:

    pip install -e .[plot]

Documentation
-------------

The documentation of SOOMpy can be found at http://sdii.ce.sc.edu/soompy_docs/.

Examples
--------

Currently, there is only one example program:

 - **point_plot.py** - Shows basic plotting of points with matplotlib.


   [req-numpy]: http://www.numpy.org/
   [req-scipy]: http://www.scipy.org/
   [pip]: http://pip.readthedocs.org/en/latest/