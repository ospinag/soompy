# -*- coding: utf-8 -*-
"""
3DOF Example
~~~~~~~~~~~~

This 3DOF example is the traditional mass, spring and damper model.  The first
node is constrains while the other nodes are free to translate in the along the
z-axis only.
"""

from soom.fem.node import Node
from soom.fem.femmodel import FemModel
from soom.fem.lspring import Lspring
from soom.fem.vdamper import Vdamper
from soom.fem.lmass import Lmass

# List of nodes
nlist = [Node([0,0,0], bc = [True]*6), 
         Node([0,0,1], bc = [True, True, False, True, True, True]), 
         Node([0,0,2], bc = [True, True, False, True, True, True]),
         Node([0,0,3], bc = [True, True, False, True, True, True])]

# Elements
ele = [Lspring([nlist[0], nlist[1]], [90, 0, 0, 0, 0, 0]),
       Lspring([nlist[1], nlist[2]], [50, 0, 0, 0, 0, 0]),
       Lspring([nlist[2], nlist[3]], [70, 0, 0, 0, 0, 0]),
       Vdamper([nlist[0], nlist[1]], [9, 0, 0, 0, 0, 0]),
       Vdamper([nlist[1], nlist[2]], [5, 0, 0, 0, 0, 0]),
       Vdamper([nlist[2], nlist[3]], [7, 0, 0, 0, 0, 0]),
       Lmass([nlist[0], nlist[1]],[10, 10, 10, 0, 0, 0]),
       Lmass([nlist[1], nlist[2]],[10, 10, 10, 0, 0, 0]),
       Lmass([nlist[2], nlist[3]],[10, 10, 10, 0, 0, 0])]

# Creating the model
mymodel = FemModel(ele,'3DOF')

# Numbering the nodes of the element
mymodel.numberNodes()

# Calculating stiffness and damping matrices
KK = mymodel.globalk()
CC = mymodel.globalc()
MM = mymodel.globalm()


